* Ehsan Jahangirzadeh 810194554
* CA3 static CMOS

**** Load libraries ****
.inc '45nm.pm'
.vec 'Vector.txt'

**** Parameters ****
.param Lmin=45n
+Beta=2
+Vdd=1.1V

.param slp=1p
.param Out_T_DLY=12.95
.param PRD=13

.temp   25

**** Source Voltage ****

VSupply		Vs		gnd		DC		Vdd

** uncomment for test
* Vin1		X		gnd		pulse	0	Vdd	0	1p	1p	80p  160p
* Vin2		A		gnd		pulse	0	Vdd	40p	1p	1p	80p  160p
* Vclk1		clk		gnd		pulse	0	Vdd	0	1f	1f	60p  90p

** uncomment for times 1
* Vin1		X		gnd		pulse	0	Vdd	130p	1p	1p	40p  80p 
* Vin2		A		gnd		DC		Vdd
* Vclk1		clk		gnd		pulse	0	Vdd	0		1f	1f	40p  80p 

** uncomment for power analysis
Vclk1		clk		gnd		pulse	0	Vdd	0	1f	1f	60p  90p 

**** Subkits ****

**** Static CMOS Inverter ****
.SUBCKT Inverter IN VDD GND OUT
M1	OUT	IN	VDD	VDD	pmos	l='Lmin'	w='2*Lmin*Beta'	
M2	OUT	IN	GND	GND	nmos	l='Lmin'	w='2*Lmin'
.ENDS Inverter

**** Dynamic And ****
.SUBCKT AND IN1 IN2 CLK VDD GND OUT
M3	OUT		OUT1	VDD		VDD		pmos	l='Lmin'	w='2*Lmin*Beta'	
M4	OUT		OUT1	GND		GND		nmos	l='Lmin'	w='2*Lmin'
M5	OUT1	CLK		VDD		VDD		pmos	l='Lmin'	w='2*Lmin*Beta'
M6	OUT4	CLK		GND		GND		nmos	l='Lmin'	w='2*Lmin'		
M7	OUT1	IN1		a1		a1		nmos	l='Lmin'	w='2*Lmin'
M8	a1		IN2		OUT4	OUT4	nmos	l='Lmin'	w='2*Lmin'
.ENDS AND

**** Circuit ****
X1	X		Vs	gnd	out1	Inverter
X2	out1	Vs	gnd	out2	Inverter
X3	out2	A	clk	Vs		gnd	Y1	AND
X4	out1	A	clk	Vs		gnd	Y2	AND

**** Analysis ****
.tran 	1p 500p 

**** Measurements ****
.meas tran 	AVGpower avg 	Power	from=0	to=500p

.end