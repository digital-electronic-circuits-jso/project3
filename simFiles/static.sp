* Ehsan Jahangirzadeh 810194554
* CA3 static CMOS

**** Load libraries ****
.inc '45nm.pm'
.vec 'Vector.txt'

**** Parameters ****
.param Lmin=45n
+Beta=2
+Vdd=1.1V

.param slp=0.1p
.param Out_T_DLY=12.95
.param PRD=13

.temp   25

**** Source Voltage ****

VSupply		Vs		gnd		DC		Vdd

** uncomment for test
* Vin1		X		gnd		pulse	0	Vdd	0	1p	1p	80p  160p
* Vin2		A		gnd		pulse	0	Vdd	40p	1p	1p	80p  160p

** uncomment for times 1
* Vin1		X		gnd		pulse	0	Vdd	0	1p	1p	40p  80p
* Vin2		A		gnd		DC		Vdd
** uncomment for times 2
* Vin1		A		gnd		pulse	0	Vdd	0	1p	1p	40p  80p
* Vin2		X		gnd		DC		Vdd

** comment all for power

**** Subkits ****

**** Static CMOS Inverter ****
.SUBCKT Inverter IN VDD GND OUT
M1	OUT	IN	VDD	VDD	pmos	l='Lmin'	w='2*Lmin*Beta'	
M2	OUT	IN	GND	GND	nmos	l='Lmin'	w='2*Lmin'
.ENDS Inverter

**** Static CMOS And ****
.SUBCKT AND IN1 IN2 VDD GND OUT
M3	OUT		OUT1	VDD	VDD	pmos	l='Lmin'	w='2*Lmin*Beta'	
M4	OUT		OUT1	GND	GND	nmos	l='Lmin'	w='2*Lmin'
M5	OUT1	IN1		VDD	VDD	pmos	l='Lmin'	w='2*Lmin*Beta'	
M6	OUT1	IN2		VDD	VDD	pmos	l='Lmin'	w='2*Lmin*Beta'	
M7	OUT1	IN1		a1	a1	nmos	l='Lmin'	w='2*Lmin'
M8	a1		IN2		GND	GND	nmos	l='Lmin'	w='2*Lmin'
.ENDS AND

**** Circuit ****
X1	X		Vs	gnd	out1	Inverter
X2	out1	Vs	gnd	out2	Inverter
X3	out2	A	Vs	gnd		Y1	AND
X4	out1	A	Vs	gnd		Y2	AND

**** Analysis ****
.tran 	1p 300p 

**** Measurements ****
.meas tran 	AVGpower avg 	Power	from=0	to=300p

.end